/*
===============================================================================

  FILE:  lasexample_write_FWFex.cpp
  
  CONTENTS:
  
    This source code serves as an example how you can easily use LASlib to
    write to the LAS format or - its compressed, but identical twin - the
    LAZ format and include additional per point attributes that are formally
    documented using the "extra bytes" functionality introduced in the 1.4
    version of the LAS specification...and FWF data.

  PROGRAMMERS:

    martin.isenburg@rapidlasso.com  -  http://rapidlasso.com
    eric.rehm@gmail.com      

  COPYRIGHT:

    (c) 2007-2018, martin isenburg, rapidlasso - fast tools to catch reality

    This is free software; you can redistribute and/or modify it under the
    terms of the GNU Lesser General Licence as published by the Free Software
    Foundation. See the LICENSE.txt file for more information.

    This software is distributed WITHOUT ANY WARRANTY and without even the
    implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  
  CHANGE HISTORY:
  
    22 December 2017 -- created after question of Jung (from China?)
    14 January 2019 -- Add synthetic FWF example
  
===============================================================================
*/

#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "laswriter.hpp"
#include "laswaveform13writer.hpp"

void usage(bool wait=false)
{
  fprintf(stderr,"usage:\n");
  fprintf(stderr,"lasexample_write_FWFex out.las\n");
  fprintf(stderr,"lasexample_write_FWFex -o out.las -verbose\n");
  fprintf(stderr,"lasexample_write_FWFex -o out.las -verbose -waveform\n");
  fprintf(stderr,"lasexample_write_FWFex > out.las\n");
  fprintf(stderr,"lasexample_write_FWFex -h\n");
  if (wait)
  {
    fprintf(stderr,"<press ENTER>\n");
    getc(stdin);
  }
  exit(1);
}

static void byebye(bool error=false, bool wait=false)
{
  if (wait)
  {
    fprintf(stderr,"<press ENTER>\n");
    getc(stdin);
  }
  exit(error);
}

static double taketime()
{
  return (double)(clock())/CLOCKS_PER_SEC;
}

int main(int argc, char *argv[])
{
  int i, j, idx;
  int Npoints_to_write = 100;
  int Nreturns_per_point = 1;
  bool verbose = false;
  bool waveform = false;
  double start_time = 0.0;

  LASwriteOpener laswriteopener;
  LASwaveform13writer* laswaveform13writer = 0;

  if (argc == 1)
  {
    fprintf(stderr,"%s is better run in the command line\n", argv[0]);
    char file_name[256];
    fprintf(stderr,"enter output file: "); fgets(file_name, 256, stdin);
    file_name[strlen(file_name)-1] = '\0';
    laswriteopener.set_file_name(file_name);
  }
  else
  {
    laswriteopener.parse(argc, argv);
  }

  for (i = 1; i < argc; i++)
  {
    if (argv[i][0] == '\0')
    {
      continue;
    }
    else if (strcmp(argv[i],"-h") == 0 || strcmp(argv[i],"-help") == 0)
    {
      usage();
    }
    else if (strcmp(argv[i],"-v") == 0 || strcmp(argv[i],"-verbose") == 0)
    {
      verbose = true;
    }
    else if (i == argc - 1 && !laswriteopener.active())
    {
      laswriteopener.set_file_name(argv[i]);
    }
    else if (strcmp(argv[i],"-waveform") == 0 || strcmp(argv[i],"-waveforms") == 0)
    {
      waveform = true;
    }
    else
    {
      fprintf(stderr, "ERROR: cannot understand argument '%s'\n", argv[i]);
      usage();
    }
  }

  if (verbose) start_time = taketime();

  // check output

  if (!laswriteopener.active())
  {
    fprintf(stderr,"ERROR: no output specified\n");
    usage(argc == 1);
  }

  // init header

  LASheader lasheader;
  lasheader.version_major = 1;
  lasheader.version_minor = 3;
  lasheader.global_encoding = 1;
  lasheader.x_scale_factor = 0.01;
  lasheader.y_scale_factor = 0.01;
  lasheader.z_scale_factor = 0.01;
  lasheader.x_offset =  500000.0;
  lasheader.y_offset = 4100000.0;
  lasheader.z_offset = 0.0;
  if (!waveform) {
        lasheader.point_data_format = 1;
        lasheader.point_data_record_length = 28;
  } else {
        lasheader.point_data_format = 4;    // format 1 --> 4 adding wave packets
        lasheader.point_data_record_length = 28 + 29;
        Npoints_to_write = 1;
        Nreturns_per_point = 3;
  }

  // add two attributes

  // type = 0 : unsigned char
  // type = 1 : char
  // type = 2 : unsigned short
  // type = 3 : short
  // type = 4 : unsigned int
  // type = 5 : int
  // type = 6 : unsigned int64
  // type = 7 : int64
  // type = 8 : float  (try not to use)
  // type = 9 : double (try not to use)

  I32 attribute_index_echo_width = -1;
  I32 attribute_index_height_above_ground = -1;

  // first an unsigned short that expresses echo-width in nanoseconds [ns]

  F64 echo_width_scale = 0.1;
  F64 echo_width_offset = 0.0;

  try {
    I32 type = 2; // unsigned short
    LASattribute attribute(type, "echo width", "full width at half maximum [ns]");
    attribute.set_scale(echo_width_scale);
    attribute.set_offset(echo_width_offset);
    attribute_index_echo_width = lasheader.add_attribute(attribute);
  }
  catch(...) {
    fprintf(stderr,"ERROR: initializing first additional attribute\n");
    usage(argc == 1);
  }

  // second an signed short that expresses height above ground in meters [m]

  F64 height_above_ground_scale = 0.01;
  F64 height_above_ground_offset = 300.0;

  try {
    I32 type = 3; // signed short
    LASattribute attribute(type, "height above ground", "vertical distance to TIN [m]");
    attribute.set_scale(height_above_ground_scale);
    attribute.set_offset(height_above_ground_offset);
    attribute_index_height_above_ground = lasheader.add_attribute(attribute);
  }
  catch(...) {
    fprintf(stderr,"ERROR: initializing second additional attribute\n");
    usage(argc == 1);
  }

  // create extra bytes VLR

  lasheader.update_extra_bytes_vlr();

  // add number of extra bytes to the point size
  
  lasheader.point_data_record_length += lasheader.get_attributes_size();

  // get indices for fast extra bytes access

  I32 attribute_start_echo_width = lasheader.get_attribute_start(attribute_index_echo_width);
  I32 attribute_start_height_above_ground = lasheader.get_attribute_start(attribute_index_height_above_ground);
    
  // Update header with waveform information
  // Based on ASPRSorg Wiki example :
  // https://github.com/ASPRSorg/LAS/wiki/Waveform-Data-Packet-Descriptors-Explained
    
    F32 location[] = {23355.3, 49297.5, 78432.8};           // ps
    F32 XYZt[3] =  {-4.9922e-5, 5.89773e-6, 0.000141175};   // (m/ps) No refraction
    F64 XYZreturn[3];  // location of returns
    F64 XYZS0[3];      // start of digitized waveform
    U8* samples = NULL;
    U64 offset;
    U32 size;

    if (waveform) {
        // Create an array of 256 descriptors (following lasreader_las.cpp)
        lasheader.vlr_wave_packet_descr = new LASvlr_wave_packet_descr*[256];
        for (j = 0; j < 256; j++) lasheader.vlr_wave_packet_descr[j] = 0;

        // Create another VLR for our one and only wave packet descriptor and hook it up
        idx = 1;
        U16 record_length_after_header = 26;
        U8* data = new U8[record_length_after_header];
        char description[50];
        sprintf(description, "Wave Packet Descriptor %d", idx);
        lasheader.add_vlr("LASF_Spec", 99+idx, record_length_after_header, data, FALSE, description, FALSE);
        lasheader.vlr_wave_packet_descr[idx] = (LASvlr_wave_packet_descr*)lasheader.vlrs[1].data;

        // Initialize first descriptor only (as per ASPRSorg Wiki example)
        U8 nbits = 8;
        U8 compression = 0;
        U32 nsamples = 88;
        U32 temporalSpacing = 1000;  // ps
        F64 digitizerGain   = 0.0086453;
        F64 digitizerOffset = -0.1326341;
        lasheader.vlr_wave_packet_descr[idx]->setBitsPerSample(nbits) ;
        lasheader.vlr_wave_packet_descr[idx]->setCompressionType(compression);
        lasheader.vlr_wave_packet_descr[idx]->setNumberOfSamples(nsamples);
        lasheader.vlr_wave_packet_descr[idx]->setTemporalSpacing(temporalSpacing);
        lasheader.vlr_wave_packet_descr[idx]->setDigitizerGain(digitizerGain);
        lasheader.vlr_wave_packet_descr[idx]->setDigitizerOffset(digitizerOffset);

        // Find start of first return: S0 = R0 + L1 * V
        //    where S0 = {sx, sy, sz}
        //          R0 = {px, py, pz}
        //          V  = {dx,dy,dz}
        //          L0 = return point location L_0 (return 1 of N)
        
        // Find end of first return : E0 = S0 - Ns * dt * V
        //    where E0 = {ex, ey, ez}
        //          S0 = {sx, sy, sz}  (from above)
        //          V  = {dx,dy,dz}
        //          Ns = nsamples
        //          dt = temporalSpacing

        // Each return itself
        // If you have S0, then Ri = S0 - Li*V  for any i

        // HACK: Using ASPRSorg example data, back-calculate start of digitized waveform
        XYZS0[0] =  235374.66 + XYZt[0]*location[0];
        XYZS0[1] = 5800928.34 + XYZt[1]*location[0];
        XYZS0[2] =     276.50 + XYZt[2]*location[0];

        // Allocate waveform data storage
        if (samples) delete [] samples;
        size = (nbits/8) * nsamples;
        samples = new U8[size];
        memset(samples, 0, size);
        
        // Create a fake spike sample at time / location of each return
        int ireturn;
        for (j = 0; j < Nreturns_per_point; j++)
        {
            ireturn = (int) round(location[j] / (F32) temporalSpacing);
            samples[ireturn] = (1<<6) * (Nreturns_per_point - j);  // Fake
            printf("samples[%d] = %x\n", ireturn, samples[ireturn]);
        }
    }

  // init point

  LASpoint laspoint;
  laspoint.init(&lasheader, lasheader.point_data_format, lasheader.point_data_record_length, 0);

  // open laswriter

  LASwriter* laswriter = laswriteopener.open(&lasheader);
  if (laswriter == 0)
  {
    fprintf(stderr, "ERROR: could not open laswriter\n");
    byebye(argc==1);
  }
    
  // open waveformwriter after laswriter(Borrowed from laszip.cpp)
    if (waveform)
    {
        laswaveform13writer = laswriteopener.open_waveform13(&lasheader);
        if (laswaveform13writer == 0)
        {
            fprintf(stderr, "ERROR: could not open laswaveform13writer\n");
            byebye(argc==1);
        }
    }
    
  if (verbose) fprintf(stderr, "writing %d points to '%s'.\n",
                       Npoints_to_write, laswriteopener.get_file_name());

  // write points

  F32 echo_width;
  F32 height_above_ground;
  I32 temp_i;

    for (i = 0; i < Npoints_to_write; i++)
    {
        for (j = 0; j< Nreturns_per_point; j++) {
            // populate the point
            
            laspoint.set_intensity((U16)i);
            laspoint.set_gps_time(23365829.0 + 0.0006*i);
            
            // Add waveform attributes
            if (!waveform)
            {
                laspoint.set_X(i);
                laspoint.set_Y(i);
                laspoint.set_Z(i);
            }
            else
            {
                XYZreturn[0] = XYZS0[0] - location[j]*XYZt[0];   // Notice minus sign
                XYZreturn[1] = XYZS0[1] - location[j]*XYZt[1];   // Notice minus sign
                XYZreturn[2] = XYZS0[2] - location[j]*XYZt[2];   // Notice minus sign
                laspoint.set_x(XYZreturn[0]);
                laspoint.set_y(XYZreturn[1]);
                laspoint.set_z(XYZreturn[2]);
                laspoint.set_return_number((U8) j+1);
                laspoint.set_number_of_returns((U8) Nreturns_per_point);
                
                laspoint.wavepacket.setIndex((U8) idx);
                // laspoint.wavepacket.setOffset(offset) // Set by LASwaveform13writer::write_waveform
                // laspoint.wavepacket.setSize(size)     // Set by LASwaveform13writer::write_waveform
                laspoint.wavepacket.setLocation(location[j]);
                laspoint.wavepacket.setXt(XYZt[0]);
                laspoint.wavepacket.setYt(XYZt[1]);
                laspoint.wavepacket.setZt(XYZt[2]);
                
                printf("%u t %g x %g y %g z %g i %d (%d of %d) d %d e %d c %d s %d %u p %d \012", (U32)i, laspoint.get_gps_time(), laspoint.get_x(), laspoint.get_y(), laspoint.get_z(), laspoint.get_intensity(), laspoint.get_return_number(), laspoint.get_number_of_returns(), laspoint.get_scan_direction_flag(), laspoint.get_edge_of_flight_line(), laspoint.get_classification(), laspoint.get_scan_angle_rank(), laspoint.get_user_data(), laspoint.get_point_source_ID());
            }
            
            // create additional attributes for each point / return
            
            echo_width = 4.0f + 0.1f*(rand()%20);
            height_above_ground = -2.0f + 0.01f*(rand()%3000);
            
            // populate the point's extra bytes
            
            temp_i = I32_QUANTIZE((echo_width-echo_width_offset)/echo_width_scale);
            if ((temp_i < U16_MIN) || (temp_i > U16_MAX))
            {
                fprintf(stderr, "WARNING: attribute 'echo width' of type U16 is %d. clamped to [%d %d] range.\n", temp_i, U16_MIN, U16_MAX);
                laspoint.set_attribute(attribute_start_echo_width, U16_CLAMP(temp_i));
            }
            else
            {
                laspoint.set_attribute(attribute_start_echo_width, ((U16)temp_i));
            }
            
            temp_i = I32_QUANTIZE((height_above_ground - height_above_ground_offset)/height_above_ground_scale);
            if ((temp_i < I16_MIN) || (temp_i > I16_MAX))
            {
                fprintf(stderr, "WARNING: attribute 'height above ground' of type I16 is %d. clamped to [%d %d] range.\n", temp_i, I16_MIN, I16_MAX);
                laspoint.set_attribute(attribute_start_height_above_ground, I16_CLAMP(temp_i));
            }
            else
            {
                laspoint.set_attribute(attribute_start_height_above_ground, ((I16)temp_i));
            }
            
            // write the waveform only on the first return because
            // we are sharing one waveform descriptor packet (WDP) with all three returns
            // (Side effect of this call is to calculate the wavepacket offset and size
            // which will stay the same for subsequent points.)
            if (waveform && (j == 0))
            {
                laswaveform13writer->write_waveform(&laspoint, samples);
            }
            offset = laspoint.wavepacket.getOffset();
            size = laspoint.wavepacket.getSize();
            // printf("offset: %lld, size: %d\n", offset, size);

            // write the point
            
            laswriter->write_point(&laspoint);
            
            // add it to the inventory
            
            laswriter->update_inventory(&laspoint);
        }
    }

  // update the header

  laswriter->update_header(&lasheader, TRUE);
    
  // close the writer

  I64 total_bytes = laswriter->close();

  if (waveform)
  {
      laswaveform13writer->close();
  }
      

#ifdef _WIN32
  if (verbose) fprintf(stderr,"total time: %g sec %I64d bytes for %I64d points\n", taketime()-start_time, total_bytes, laswriter->p_count);
#else
  if (verbose) fprintf(stderr,"total time: %g sec %lld bytes for %lld points\n", taketime()-start_time, total_bytes, laswriter->p_count);
#endif

  delete laswriter;

  return 0;
}
