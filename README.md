# LAStoolsSN

Sentinelle Nord LiDAR processing tools based on rapidlasso LAStools, LASzip, and PulseWaves.

Depends on LAStools from www.rapidlasso.com

```shell
mkdir src
cd src

# get latest rapidlasso LAStools
git clone https://github.com/LAStools/LAStools.git
cd LAStools
make

# git Sentinelle Nord rapidlasso tools
cd ..
git clone https://gitlab.com/SentinelleNord/OAS/LAStoolsSN.git
cd LAStoolsSN
```
